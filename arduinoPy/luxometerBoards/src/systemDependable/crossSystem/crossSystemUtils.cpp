#ifndef  CROSS_SYSTEM_UTILS_H
#define  CROSS_SYSTEM_UTILS_H


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
// define something for Windows (32-bit and 64-bit, this part is common)
#ifdef _WIN64
// define something for Windows (64-bit only)
#else
// define something for Windows (32-bit only)
#endif
#elif __APPLE__
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
// iOS, tvOS, or watchOS Simulator
#elif TARGET_OS_MACCATALYST
// Mac's Catalyst (ports iOS API into Mac, like UIKit).
#elif TARGET_OS_IPHONE
// iOS, tvOS, or watchOS device
#elif TARGET_OS_MAC
// Other kinds of Apple platforms
#else
#error "Unknown Apple platform"
#endif
#elif __linux__
#define LINUX_PLATFORM
// linux
#elif __unix__ // all unices not caught above
#define LINUX_PLATFORM
// Unix
#elif defined(_POSIX_VERSION)
#define POSIX_PLATFORM
// POSIX
#elif defined(ARDUINO)
#if ARDUINO >= 100
#include "Arduino.h"
#define ARDUINO_PLATFORM
#else
#include "WProgram.h"
#define ARDUINO_PLATFORM
#endif
#else
#error "Unknown compiler"
#endif


// #if defined(LINUX_PLATFORM)
// #include <chrono>
// unsigned long
// millis() {
//   std::chrono::time_point<std::chrono::system_clock> now =
//     std::chrono::system_clock::now();
//   auto duration = now.time_since_epoch();
//   auto millis =
//     std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
//   return millis;
// }
// #elif defined(ARDUINO_PLATFORM)
// //Arduino have it's own millis
// #endif

#endif
