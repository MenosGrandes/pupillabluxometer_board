#ifndef GY49_LUXOMETER_HPP
#define GY49_LUXOMETER_HPP

#include <Arduino.h>
#include <Wire.h>
#include <stdint.h>

constexpr uint8_t Addr = 0x4A;
class GY49
{
private:
public:
  void init()
  {
    // Initialise I2C communication as MASTER
    Wire.begin();
    // Start I2C Transmission
    Wire.beginTransmission(Addr);
    // Select configuration register
    Wire.write(0x02);
    // Continuous mode, Integration time = 800 ms
    Wire.write(0x40);
    // Stop I2C transmission
    Wire.endTransmission();
    Serial.println(F("Found a GY49 sensor"));
    delay(300);
  }

  float read(void)
  {
    unsigned int data[2];

    // Start I2C Transmission
    Wire.beginTransmission(Addr);
    // Select data register
    Wire.write(0x03);
    // Stop I2C transmission
    Wire.endTransmission();

    // Request 2 bytes of data
    Wire.requestFrom(Addr, 2);

    // Read 2 bytes of data
    // luminance msb, luminance lsb
    if (Wire.available() == 2)
    {
      data[0] = Wire.read();
      data[1] = Wire.read();
      // Convert the data to lux
      int exponent = (data[0] & 0xF0) >> 4;
      int mantissa = ((data[0] & 0x0F) << 4) | (data[1] & 0x0F);
      const float luminance = pow(2, exponent) * mantissa * 0.045;
      return luminance;
    }
    return 0;
  }
  /*
  void configure(DFRobot_GY49::als_gain_t gain,
                 DFRobot_GY49::als_itime_t integrationTime) {
    sensor.setGain(gain);
    sensor.setIntegrationTime(integrationTime);
  }*/
};

#endif
