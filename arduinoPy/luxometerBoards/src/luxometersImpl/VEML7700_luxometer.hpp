#ifndef VEML7700_LUXOMETER_HPP
#define VEML7700_LUXOMETER_HPP

/*
 *https://github.com/adafruit/Adafruit_VEML7700
 * */
#include <Adafruit_VEML7700.h>
#include <Arduino.h>
#include <Wire.h>
#include <stdint.h>

class VEML7700
{
  private:
    Adafruit_VEML7700 sensor;

  public:
    VEML7700() { sensor = Adafruit_VEML7700(); }
    void init()
    {
        if (sensor.begin())
        {
            Serial.println(F("Found!"));
        }
        else
        {
            Serial.println(("No sensor found ... check your wiring?"));
            while (1)
                ;
        }
        sensor.setGain(VEML7700_GAIN_1);
        sensor.setIntegrationTime(VEML7700_IT_800MS);
    }

    float read(void) { return sensor.readLux(); }
    /*
    void configure(VEML770Gain_t gain, tsl2591IntegrationTime_t integrationTime)
    { sensor.setGain(gain); sensor.setTiming(integrationTime);
    }*/
};

#endif
