#ifndef TSL2591_LUXOMETER_HPP
#define TSL2591_LUXOMETER_HPP

#include <Adafruit_TSL2591.h>
#include <Arduino.h>
#include <Wire.h>
#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <stdint.h>
#include <FastLED.h>
class TSL2591 : public DeviceIf<TSL2591, descriptors::DESCRIPTOR_TSL2591> {
private:
  Adafruit_TSL2591 sensor;

public:
  TSL2591() {
    sensor = Adafruit_TSL2591(2591);
  }
  void init() {
    if (sensor.begin()) {
    } else {
      while (1)
        ;
    }

    configure(TSL2591_GAIN_MED, TSL2591_INTEGRATIONTIME_300MS);
  }
  ~TSL2591() {
  }

  float read(void) {
    const uint32_t lum = sensor.getFullLuminosity();
    const uint16_t ir = lum >> 16;
    const uint16_t full = lum & 0xFFFF;

    const auto lux = sensor.calculateLux(full, ir);
    return lux;
  }
  void configure(tsl2591Gain_t gain, tsl2591IntegrationTime_t integrationTime) {
    sensor.setGain(gain);
    sensor.setTiming(integrationTime);
  }
};

#endif
