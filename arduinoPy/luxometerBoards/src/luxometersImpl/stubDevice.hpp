#ifndef STUB_DEVICE_HPP
#define STUB_DEVICE_HPP

#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <logger.cpp>
struct StubDevice : public DeviceIf<StubDevice, descriptors::DESCRIPTOR_STUB>
{
  void  init() {}
  float read(void) const
  {
    Logger::getLogger().log("StubDevice", "Returning 1.12");
    return 1.12f;
  }
};

#endif // !STUB_DEVICE_HPP
