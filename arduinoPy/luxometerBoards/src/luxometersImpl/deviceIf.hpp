#ifndef DEVICE_HPP
#define DEVICE_HPP
#include <stdint.h>
template<typename CONCRETE_DIVICE, uint8_t __descriptor,
         typename returnType = float>
struct DeviceIf {
  void init() {
    static_cast<CONCRETE_DIVICE*>(this)->init();
  }

  template<typename gain_t, typename integrationTime_t>
  void configure(gain_t gain, integrationTime_t integrationTime) {
    static_cast<CONCRETE_DIVICE*>(this)->configure(gain, integrationTime);
  }

  returnType read() {
    static_cast<CONCRETE_DIVICE*>(this)->read();
  }

  static constexpr auto Descriptor = __descriptor;
};

#endif // !DEVICE_HPP
