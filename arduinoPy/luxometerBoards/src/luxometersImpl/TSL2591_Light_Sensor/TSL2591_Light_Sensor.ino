#include "DEV_Config.h"
#include "TSL2591.h"

UWORD Lux = 0;
void setup() {
 Serial.begin(9600); //9600
DEV_ModuleInit(9600);
TSL2591_Init();
}

void loop() {
    Lux = TSL2591_Read_Lux();
    Serial.println(Lux);

    TSL2591_SET_LuxInterrupt(50,200);
}
