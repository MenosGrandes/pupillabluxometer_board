#ifndef TSL2561_LUXOMETER_HPP
#define TSL2561_LUXOMETER_HPP

#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561.h>
#include <Arduino.h>
#include <Wire.h>
#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <logger.cpp>
#include <stddef.h>
#include <stdint.h>
class TSL2561 : public DeviceIf<TSL2561, descriptors::DESCRIPTOR_TSL2561, uint16_t> {
private:
  Adafruit_TSL2561_Unified sensor{ 0, 0 };
  static constexpr uint32_t SENSOR_ID{ 2561 };

public:
  TSL2561()
    : sensor(Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, SENSOR_ID)) {}
  void init() {
    if (sensor.begin()) {
    } else {
      while (1)
        ;
    }
  }

  uint16_t read(void) {
    uint16_t ir;
    uint16_t broadband;
    sensor.getLuminosity(&broadband, &ir);
    return broadband;
  }
  void configure(tsl2561Gain_t gain, tsl2561IntegrationTime_t integrationTime) {
    sensor.setGain(gain);
    sensor.setIntegrationTime(integrationTime);
  }
};

#endif
