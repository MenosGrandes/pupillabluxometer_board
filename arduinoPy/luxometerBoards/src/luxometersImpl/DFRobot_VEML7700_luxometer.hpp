#ifndef VEML7700_LUXOMETER_HPP
#define VEML7700_LUXOMETER_HPP

#include <Arduino.h>
#include <DFRobot_VEML7700.h>
#include <Wire.h>
#include <stdint.h>

class VEML7700
{
private:
  DFRobot_VEML7700 sensor;

public:
  void init()
  {
    sensor.begin();
    Serial.println(F("Found a VEML7700 sensor"));
  }

  float read(void)
  {
    float lux;
    sensor.getALSLux(lux);
    return lux;
  }
  void configure(DFRobot_VEML7700::als_gain_t gain,
                 DFRobot_VEML7700::als_itime_t integrationTime)
  {
    sensor.setGain(gain);
    sensor.setIntegrationTime(integrationTime);
  }
};

#endif
