#ifndef EX_PROTOCOL_HANDLER_HPP
#define EX_PROTOCOL_HANDLER_HPP
#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <exProtocol.h>
#include <logger.cpp>
#include <pb_common.h>
#include <pb_decode.h>
#include <pb_encode.h>
#include <protocolHandlerIf.hpp>

namespace tag
{
struct EXMessageFetchLuxRequestTag
{
};
struct EXMessageSetupRequestTag
{
};
struct EXMessageSetupResponseTag
{
};
struct EXMessageFetchLuxResponseTag
{
};
struct UNKNOWN
{
};
} // namespace tag

template <typename DeviceIf>
class EXProtocolHandler : public ProtocolHandlerIf<EXProtocolHandler<DeviceIf>>
{
private:
public:
  EXProtocolHandler(DeviceIf *_device)
      : luxometerDevice(_device)
      , connected(connected_t::NOT){};
  EXProtocolHandler()                          = delete;
  EXProtocolHandler(EXProtocolHandler &&)      = delete;
  EXProtocolHandler(const EXProtocolHandler &) = delete;
  EXProtocolHandler &operator=(EXProtocolHandler &&) = delete;
  EXProtocolHandler &operator=(const EXProtocolHandler &) = delete;
  ~EXProtocolHandler()                                    = default;

public:
  response_types::response connect(uint8_t *buffer, size_t &size_of_message)
  {

    EXMessage msg     = EXMessage_init_zero;
    msg.which_payload = EXMessage_setupRequest_tag;
    msg.payload.setupRequest.descriptor =
        encodeDeviceDescriptor(DeviceIf::Descriptor);
    return encodeMessage(&msg, buffer, size_of_message, true);
  }

  response_types::response disconnect()
  {
    EXMessage msg                   = EXMessage_init_zero;
    msg.which_payload               = EXMessage_setupResponse_tag;
    msg.payload.setupResponse       = EXSetupResponse_init_default;
    msg.payload.setupResponse.valid = Valid_YES;
    return response_types::response::OK;
  };

  response_types::response handleMessage(const EXMessage *msg,
                                         uint8_t *        msgBuffer,
                                         size_t &         messageLength) const
  {

    if (isConnected() == connected_t::NOT) {

      switch (msg->which_payload) {

        case EXMessage_setupResponse_tag:
          {
            return _handleMessage(msg, tag::EXMessageSetupResponseTag{});
          }

        default:
          {
            return _handleMessage(msg);
          }
      }
    }
    else {

      switch (msg->which_payload) {

        case EXMessage_setupRequest_tag:
          {
            return _handleMessage(msg, tag::EXMessageSetupRequestTag{});
          }
        case EXMessage_fetchLuxReq_tag:
          {
            return _handleMessage(msg, msgBuffer, messageLength,
                                  tag::EXMessageFetchLuxRequestTag{});
          }
        case EXMessage_fetchLuxRes_tag:
          {
            return _handleMessage(msg, tag::EXMessageFetchLuxResponseTag{});
          }
        default:
          {
            return _handleMessage(msg);
          }
      }
    }

    return response_types::response::NOK;
  }
  response_types::response decodeMessage(uint8_t *msgBuffer,
                                         size_t & messageLength) const
  {
    pb_istream_t readStream = pb_istream_from_buffer(msgBuffer, messageLength);
    EXMessage    msg{};
    if (!pb_decode(&readStream, EXMessage_fields, &msg)) {

      Logger::getLogger().log("EXProtocolHandler", "ERROR IN DECODE MESSAGE");
      return response_types::response::NOK;
    }

    Logger::getLogger().log("EXProtocolHandler",
                            "decodeMessage called with payload = %d",
                            msg.which_payload);
    return handleMessage(&msg, msgBuffer, messageLength);
  }

  response_types::response encodeMessage(void *msg, uint8_t *buffer,
                                         size_t &size_of_message,
                                         bool    needResponse) const
  {
    size_of_message = 0;
    /*
     *The second parameter of pb_ostream_from_buffer is always the max size of
     *the massage. In this case is always set to EXMessage_size
     * */
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, EXMessage_size);
    bool         status = pb_encode(&stream, EXMessage_fields, msg);
    size_of_message     = stream.bytes_written;
    if (!status) {
      return response_types::response::NOK;
    }
    if (needResponse) {
      return response_types::response::SEND_RESPONSE;
    }

    return response_types::response::OK;
  }

  EXDescriptor encodeDeviceDescriptor(const uint8_t descriptor) const
  {
    switch (descriptor) {
      case descriptors::DESCRIPTOR_TSL2561:
        return EXDescriptor_Luxometr_2561;
      case descriptors::DESCRIPTOR_STUB:
        return EXDescriptor_Luxometr_STUB;
      case descriptors::DESCRIPTOR_TSL2591:
        return EXDescriptor_Luxometr_2591;
      default:
        return EXDescriptor_Luxometr_UNKNOWN;
    }
  }
  connected_t isConnected() const
  {
    return connected;
  }

  static constexpr size_t MAX_MSG_SIZE{EXMessage_size};

private:
  mutable connected_t connected{connected_t::NOT};
  DeviceIf *          luxometerDevice{nullptr};

  response_types::response _handleMessage(const EXMessage *msg) const
  {
    Logger::getLogger().log("EXProtocolHandler", "UNKNOWN");
    return response_types::response::NOK;
  }

  response_types::response _handleMessage(const EXMessage *msg,
                                          tag::UNKNOWN) const
  {

    Logger::getLogger().log("EXProtocolHandler", "UNKNOWN 2");
    return response_types::response::NOK;
  }

  response_types::response _handleMessage(const EXMessage *msg,
                                          tag::EXMessageSetupRequestTag) const
  {

    Logger::getLogger().log("EXProtocolHandler", "SetupRequestTag");
    return response_types::response::NOK;
  }
  response_types::response _handleMessage(const EXMessage *msg,
                                          tag::EXMessageSetupResponseTag) const
  {
    Logger::getLogger().log("EXProtocolHandler",
                            "handle EXMessageSetupResponseTag");
    if (msg->payload.setupResponse.valid == Valid_YES) {

      this->connected = connected_t::YES;
      return response_types::response::OK;
    }
    else {

      this->connected = connected_t::NOT;
    }
    return response_types::response::NOK;
  }

  response_types::response
  _handleMessage(const EXMessage *msg, tag::EXMessageFetchLuxResponseTag) const
  {

    Logger::getLogger().log("EXProtocolHandler", "FetchLuxResTAG");
    return response_types::response::OK;
  }
  response_types::response
  _handleMessage(const EXMessage *msg, uint8_t *msgBuffer,
                 size_t &messageLength, tag::EXMessageFetchLuxRequestTag) const
  {

    Logger::getLogger().log("EXProtocolHandler", "FetchLuxReqTAG");
    EXMessage fetchLuxResponse                    = EXMessage_init_zero;
    fetchLuxResponse.which_payload                = EXMessage_fetchLuxRes_tag;
    const auto luxRead                            = luxometerDevice->read();
    fetchLuxResponse.payload.fetchLuxRes.luxValue = luxRead;
    return encodeMessage(&fetchLuxResponse, msgBuffer, messageLength, true);
  }
};
#endif
