
#ifndef APPIF_HPP
#define APPIF_HPP

template<typename CONCRETE_APP>
struct AppIf {
  void start() {
    preInit();
    static_cast<CONCRETE_APP*>(this)->start();
  }

  void preInit() { return static_cast<CONCRETE_APP*>(this)->preInit(); }
};

#endif // !APPIF_HPP
