#ifndef MESSAGE_HANDLER_IF
#define MESSAGE_HANDLER_IF
#include <descriptors.hpp>

template <typename CONCRETE_MESSAGE_HANDLER, class DeviceIf, class SenderIf,
          class ProtocolIf>
#define PROPER_TYPE static_cast<CONCRETE_MESSAGE_HANDLER *>(this)
struct MessageHandler
{
  bool decodeMessage(size_t  messageLength,
                     uint8_t msgBuffer[ProtocolIf::MAX_MSG_SIZE]) const
  {
    return static_cast<CONCRETE_MESSAGE_HANDLER *>(this)->decodeMessage(
        messageLength, msgBuffer);
  }
  bool connect() const
  {
    return PROPER_TYPE->connect();
  }
  connected_t isConnected() const
  {
    return PROPER_TYPE->isConnected();
  }
};

#endif // !MESSAGE_HANDLER_IF
