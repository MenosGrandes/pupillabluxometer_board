
#ifndef SERIAL_SENDER
#define SERIAL_SENDER
#include <Arduino.h>
#include <senderIf.hpp>
#include <stddef.h> // ARduino will be happy?  MenosGrandes TODO
#include <stdint.h>
struct SerialSender : public SenderIf<SerialSender>
{
  bool startSerialConnection(uint32_t baundRate)
  {
    Serial.begin(baundRate);
    while (!Serial) {
      ;
    }
    return true;
  }
  bool sendBuffer(uint8_t *buffer, uint8_t bufferSize) const
  {
    Serial.write(bufferSize);
    Serial.write(buffer, bufferSize);
    return true;
  }
};

#endif // !SERIAL_SENDER
