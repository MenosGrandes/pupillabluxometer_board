import pytest
import logging
import serial
import exMessageHandler as msg
import exProtocol_pb2 as gpb
import sched, time

logger = logging.getLogger(__name__)

# def test_setupRequestHandling_returnYES():
#     msgHandler = msg.EXMessageHandler()
#     logger.info("printout")
#
#     with open(f"{msg.EnvChecker.DAT_FILE_FOLDER}/test_setupRequestHandling_received.dat", "rb") as request:
#         msgSize = msg.toInt(request.read(1))
#         msgBuffer = request.read(msgSize)
#         msgHandler = msg.EXMessageHandler()
#         expectesMsg = gpb.EXMessage()
#         expectesMsg.setupResponse.valid = gpb.Valid.YES
#         assert expectesMsg == msgHandler.handleMessage(msgBuffer)
#         with open(f"{msg.EnvChecker.DAT_FILE_FOLDER}/test_setupRequestHandling_response_py.dat", "wb") as response:
#             response.write(msg.fromInt(expectesMsg.ByteSize()))
#             response.write(expectesMsg.SerializeToString())
def test_serialCommunication():
    msgHandler = msg.EXMessageHandler()
    with serial.Serial(f"{msg.EnvChecker.DAT_FILE_FOLDER}/py_port") as ser:
        while True:
            msgSize = msg.toInt(ser.read(1))
            msgBuffer = ser.read(msgSize)
            logger.info("Signal fetched!")
            response = msgHandler.handleMessage(msgBuffer)
            expectesMsg = gpb.EXMessage()
            expectesMsg.setupResponse.valid = gpb.Valid.YES
            assert response.setupResponse.valid == expectesMsg.setupResponse.valid
            # write and response

            logger.info(f"Send {response} from PY");
            ser.write(msg.fromInt(response.ByteSize()))
            ser.write(response.SerializeToString())
            # request for LUX!
            time.sleep(10)
            c = 3
            while True:
                logger.info("!@!@!@!@!@!@!@!@!@!@");
                luxMsg = gpb.EXMessage()
                luxMsg.fetchLuxReq._v = True
                logger.info(f"Sending from Python!: {luxMsg}")
                ser.write(msg.fromInt(luxMsg.ByteSize()))
                ser.write(luxMsg.SerializeToString())

                # wait for FetchLuxRespo with value!
                msgSize = msg.toInt(ser.read(1))
                msgBuffer = ser.read(msgSize)
                logger.info(f" MenosGrandes {msgSize} and {msgBuffer}")
                luxResponse = msgHandler.handleMessage(msgBuffer)
                if luxResponse is None:
                    continue
                logger.info(f"aksakaajjaja Msg after luxReq = {luxResponse}")
                #assert luxResponse.luxValue == 1.12
                time.sleep(0.1)
                logger.info("~!~!~!~!~!~!~!~!~!~!");
                c = c - 1
                if c <= 0:
                    break
            break
