import os
from typing import Optional
import serial
import exProtocol_pb2 as gpb

import logging


logger = logging.getLogger(__name__)


class EnvChecker:
    DAT_FILE_FOLDER = os.getenv("CPP_BUILD_FOLDER")
    if DAT_FILE_FOLDER is None:
        raise Exception("You forget to source the env.sh!")


def toInt(byte):
    return int.from_bytes(byte, byteorder="little")


def fromInt(_int: int):
    return _int.to_bytes(1, "little")


class EXMessageHandler:
    def __init__(self):
        self.__dispatcher = {
            "setupRequest": self.__handleSetupRequest,
            "setupResponse": self.__handleSetupResponse,
            "fetchLuxRes": self.__handleFetchLuxResponse,
        }

    def __getMsgType(self, exMessage: gpb.EXMessage) -> Optional[str]:
        return exMessage.WhichOneof("payload")

    def __handleFetchLuxResponse(self, msg: gpb.EXMessage):
        """ """
        logger.info(f"__handleFetchLuxResponse  signal:  {msg}")
        return msg

    def __handleSetupRequest(self, msg: gpb.EXMessage) -> gpb.EXMessage:
        """
        Handle received ExSetupRequest and saves the descriptor for the device
        Succes:
            return ExSetupResponse
        Failed:
            log error
        """
        logger.info(f"__handleSetupRequest with signal: \n {msg}")
        self.current_descriptor = msg.setupRequest.descriptor
        # #Construct ExSetupResponse
        setupResponse = gpb.EXMessage()
        setupResponse.setupResponse.valid = gpb.Valid.YES
        return setupResponse

    def __handleSetupResponse(self, msg):
        logger.info(f"__handleSetupResponse with signal: \n{msg}")
        return None

    def __handleError(self, msg):
        logger.error(f"ERROR with {msg}")
        return None

    def handleMessage(self, msg: bytes):
        try:
            exMessage = gpb.EXMessage()
            exMessage.ParseFromString(msg)
            msgType = self.__getMsgType(exMessage)
        except Exception as e:
            raise e
        return self.__dispatcher.get(msgType, self.__handleError)(exMessage)
