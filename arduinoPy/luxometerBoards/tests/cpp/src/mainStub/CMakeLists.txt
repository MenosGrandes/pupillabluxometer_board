set(target mainStub)
file(GLOB HPP_FILES "*.hpp")
add_library( ${target} ${HPP_FILES} )
set_target_properties( ${target} PROPERTIES LINKER_LANGUAGE CXX)
target_include_directories(${target} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")

