#include "utest.h"
#include <crossSystemUtils.cpp>
#include <exProtocol.h>
#include <exProtocolHandler.cpp>
#include <fileSender.hpp>
#include <iostream>
#include <logger.cpp>
#include <mainStub.hpp>
#include <serialMessageHandler.hpp>
#include <serialSenderStub.hpp>
#include <string>
#include <stubDevice.hpp>
UTEST_STATE();
int main(int argc, const char *const argv[])
{
  return utest_main(argc, argv);
};

UTEST(deserialization, mainTest)
{
  MainAppStub testApp;
  testApp.start();
Logger::getLogger().log("MainTest", "End of test!");
}
/*
UTEST(deserialization, SerialRead) {
  std::string portPath = "";
  if (const char* env_p = std::getenv("CPP_BUILD_FOLDER")) {
    portPath = env_p;
    Logger::log("MainTest", portPath.c_str());
  }
  SerialSenderStub sender(portPath + "cpp_port");
  StubDevice device;
  EXProtocolHandler<decltype(device)> protocol;
  SerialMassageHandler<StubDevice, SerialSenderStub, decltype(protocol)> mh{
    &device, &sender, &protocol
  };

  Logger::log("MainTest", "Wait until connected");
  ASSERT_TRUE(mh.connect());
  {
    uint8_t messageSize = 0;
    Logger::log("MainTest", "Wait for ConnectionResponse!");
    while (true) {
      sender.read(&messageSize); // should be size! Is it blocking call?
      uint8_t buffer[EXMessage_size];
      sender.readBytes(buffer, messageSize);
      ASSERT_TRUE(mh.decodeMessage(messageSize, buffer));
      Logger::log("MainTest", "ConnectionResponse received");

      break;
    }
  }
  {
    uint8_t messageSize = 0;
    Logger::log("MainTest", "Wait for LuxFetchRequest!");
    while (true) {
      sender.read(&messageSize); // should be size! Is it blocking call?
      uint8_t buffer[EXMessage_size];
      sender.readBytes(buffer, messageSize);
      std::cout << (mh.decodeMessage(messageSize, buffer));
      Logger::log("MainTest", "FetchLuxRequest received! send response!");

      break;
    }
  }
}
*/
