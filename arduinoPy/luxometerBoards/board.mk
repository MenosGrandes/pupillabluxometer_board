# This program is free software and is licensed under the same conditions as
# describe in https://github.com/sudar/Arduino-Makefile/blob/master/licence.txt

# The following can be overridden at make-time, by setting an environment
# variable with the same name. eg. BOARD_TAG=pro5v328 make

BOARD_TAG ?= uno
ARDUINO_LIBS_HOME = /home/mg/Downloads/arduino-1.8.19/hardware/arduino/avr/libraries
CXXFLAGS += -I$(ARDUINO_LIBS_HOME)/Wire/src
CXXFLAGS += -I$(ARDUINO_LIBS_HOME)/SPI/src
CXXFLAGS += -I$(ARDUINO_LIBS_HOME)/SoftwareSerial/src
