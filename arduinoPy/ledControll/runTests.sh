#!/bin/bash
cwd=$(pwd)
mkdir -p build/
cd build/
cmake -GNinja ..
ninja
if [ $? -eq 0 ]; then
   echo OK
else
   echo FAIL
   exit $?
fi

cd $cwd
# How test should run:
#
#

find -name "*.dat" -delete

bash runSockat.sh &
_socatPid=$!


echo "Starting Python TESTS!"
pytest -s tests/py/pupilLabLuxometerBoards_tests.py -o log_cli=true &
_py_test_pid=$!

sleep 1

echo "Starting CPP TESTS!"
./build/pupilLabLuxometerBoards_tests &
_cpp_test_pid=$!

tail --pid=$_py_test_pid -f /dev/null
tail --pid=$_cpp_test_pid -f /dev/null
echo 'killing socat!'
kill -9 $_socatPid
