CodeChecker check --enable-all --ignore skip.list -b "cd .. && rm -rf build && mkdir -p build/ &&  cd build/ && cmake -GNinja .. && ninja" -o codeCheckerResults
CodeChecker parse -e html ./codeCheckerResults -o ./reports_html
firefox reports_html/index.html
