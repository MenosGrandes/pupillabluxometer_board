#ifndef SERIAL_SENDER_STUB_HPP
#define SERIAL_SENDER_STUB_HPP

#include <SerialPort.hpp>
#include <fstream>
#include <iostream>
#include <logger.cpp>
#include <senderIf.hpp>
#include <stddef.h>
#include <stdint.h>
#include <string>

class SerialSenderStub : public SenderIf<SerialSenderStub>
{
public:
  SerialSenderStub(const std::string _portName)
    : portName(_portName)
    , serialPort(portName, 9600)
  {}

  ~SerialSenderStub()
  {}

  bool sendBuffer(uint8_t* buffer, uint8_t bufferSize) const
  {
    Logger::getLogger().log("SerialSenderStub", "Sending buffer!");
    serialPort.write(&bufferSize, 1);
    serialPort.write(buffer, bufferSize);
    return true;
  }

  void read(uint8_t* byte)
  {
    readBytes(byte, 1);
  }

  void readBytes(uint8_t* buffer, const uint8_t messageSize)
  {
    serialPort.read(buffer, messageSize);
  }

  bool available() const
  {
    return serialPort.available();
  }

private:
  const std::string    portName;
  mutable SimpleSerial serialPort;
};
#endif // !SERIAL_SENDER_STUB_HPP
