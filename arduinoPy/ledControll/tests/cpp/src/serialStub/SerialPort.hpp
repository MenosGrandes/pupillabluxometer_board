/*
 * File:   SimpleSerial.h
 * Author: Terraneo Federico
 * Distributed under the Boost Software License, Version 1.0.
 *
 * Created on September 10, 2009, 12:12 PM
 */

#ifndef _SIMPLESERIAL_H
#define _SIMPLESERIAL_H

#include <boost/asio.hpp>

class SimpleSerial {
public:
  SimpleSerial(std::string port, unsigned int baud_rate)
    : io()
    , serial(io, port) {
    serial.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
  }

  void writeString(std::string s) {
    boost::asio::write(serial, boost::asio::buffer(s.c_str(), s.size()));
  }
  void write(uint8_t* buffer, uint8_t bufferSize) {
    boost::asio::write(serial, boost::asio::buffer(buffer, bufferSize));
  }

  void read(uint8_t* buffer, const uint8_t lengthOfBytesToRead) {
    boost::asio::read(serial, boost::asio::buffer(buffer, lengthOfBytesToRead));
  }
  void read(uint8_t* buffer) {
    boost::asio::read(serial, boost::asio::buffer(buffer, 1));
  }

  /// @brief Returns the number of bytes available for reading from a serial
  ///        port without blocking.
  std::size_t get_bytes_available(boost::system::error_code& error) {
    error = boost::system::error_code();
    int value = 0;
#if defined(BOOST_ASIO_WINDOWS) || defined(__CYGWIN__)
    COMSTAT status;
    if (0 != ::ClearCommError(
               serial.lowest_layer().native_handle(), NULL, &status)) {
      value = status.cbInQue;
    }
    // On error, set the error code.
    else {
      error = boost::system::error_code(
        ::GetLastError(), boost::asio::error::get_system_category());
    }
#else  // defined(BOOST_ASIO_WINDOWS) || defined(__CYGWIN__)
    if (0 == ::ioctl(serial.lowest_layer().native_handle(), FIONREAD, &value)) {
      error = boost::system::error_code(
        errno, boost::asio::error::get_system_category());
    }
#endif // defined(BOOST_ASIO_WINDOWS) || defined(__CYGWIN__)

    return error ? static_cast<std::size_t>(0) : static_cast<size_t>(value);
  }

  /// @brief Returns the number of bytes available for reading from a serial
  ///        port without blocking.  Throws on error.
  std::size_t get_bytes_available() {
    boost::system::error_code error;
    std::size_t bytes_available = get_bytes_available(error);
    if (error) {
      boost::throw_exception((boost::system::system_error(error)));
    }
    return bytes_available;
  }

  bool available() { return (get_bytes_available() > 0); }

  std::string readLine() {
    using namespace boost;
    char c;
    std::string result;
    for (;;) {
      asio::read(serial, asio::buffer(&c, 1));
      switch (c) {
        case '\r':
          break;
        case '\n':
          return result;
        default:
          result += c;
      }
    }
  }

private:
  boost::asio::io_service io;
  boost::asio::serial_port serial;
};

#endif /* _SIMPLESERIAL_H */
