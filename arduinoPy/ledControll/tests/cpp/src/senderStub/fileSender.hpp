#ifndef FILE_SENDER_HPP
#define FILE_SENDER_HPP
// Dont include it inside the Arduino code!
#include <fstream>
#include <iostream>
#include <logger.cpp>
#include <senderIf.hpp>
#include <stddef.h>
#include <stdint.h>
#include <string>
struct FileSender : public SenderIf<FileSender>
{

    FileSender(const std::string fileName)
      : fileName(fileName)
    {
    }
    ~FileSender() { /*std::remove(fileName.c_str());*/ }
    bool sendBuffer(uint8_t* buffer, uint8_t bufferSize) const
    {
        std::ofstream file;
        file.open(fileName, std::ios::out | std::ios::binary);
        Logger::getLogger().log("FileSender", "sendBuffer size=%d", bufferSize);
        Logger::getLogger().log_array(
          "FileSender", "sendBuffer buffer:", buffer, bufferSize);

        file.write((char*)&bufferSize, sizeof(uint8_t));
        file.write(
          (const char*)buffer,
          bufferSize *
            sizeof(uint8_t)); // why it must be const char*?? MenosGrandes
        file.close();

        return true;
    }

    const std::string fileName;
};

#endif // !FILE_SENDER_HPP
