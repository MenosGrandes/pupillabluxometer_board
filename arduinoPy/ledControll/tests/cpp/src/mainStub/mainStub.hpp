#ifndef MAIN_STUB_HPP
#define MAIN_STUB_HPP
#include <appIf.cpp>
#include <chrono>
#include <exProtocol.h>
#include <exProtocolHandler.cpp>
#include <fileSender.hpp>
#include <iostream>
#include <logger.cpp>
#include <serialMessageHandler.hpp>
#include <serialSenderStub.hpp>
#include <string>
#include <stubDevice.hpp>
#include <thread>
#include <timer.cpp>
using namespace std::chrono_literals;
using LuxometerDevice = StubDevice;
struct MainAppStub : public AppIf<MainAppStub>
{
  int preInit()
  {
    return 0;
  }
  void start()
  {
    Logger::getLogger().log("MainAppStub", "start!");

    std::string portPath = "";
    if (const char *env_p = std::getenv("CPP_BUILD_FOLDER")) {
      portPath = env_p;
    }
//     SerialSenderStub sender(portPath + "cpp_port");
// 
//     LuxometerDevice                     device;
//     EXProtocolHandler<decltype(device)> protocol(&device);
//     SerialMassageHandler<decltype(device), decltype(sender), decltype(protocol)>
//         serialMsgHandler{&device, &sender, &protocol};
// 
//     device.init();
//     Timer<500, decltype(serialMsgHandler)> connectionTimer(
//         &serialMsgHandler,
//         &SerialMassageHandler<decltype(device), decltype(sender),
//                               decltype(protocol)>::connect);
// 
//     // get 5 valid msg and end test!
//     int msgCount{0};
//     while (true) {
// 
//       if (connected_t::NOT == protocol.isConnected()) {
// 
//         connectionTimer.update();
//       }
// 
//       if (sender.available()) {
//         uint8_t messageSize{0};
//         sender.read(&messageSize);
//         uint8_t buffer[EXMessage_size];
//         sender.readBytes(buffer, messageSize);
//         Logger::getLogger().log(
//             "MainStub", " data in Serial available. Call for DecodeMessage");
//         const bool valid = serialMsgHandler.decodeMessage(messageSize, buffer);
//         if (valid) {
//           msgCount++;
//           if (msgCount == 20) {
//             exit(0);
//           }
//         }
//         std::this_thread::sleep_for(100ms);
//       }
//     }
   }
};

#endif //
