#include "utest.h"
#include <exProtocol.h>
#include <exProtocolHandler.cpp>
#include <fileSender.hpp>
#include <iostream>
#include <logger.cpp>
#include <mainStub.hpp>
#include <serialMessageHandler.hpp>
#include <serialSenderStub.hpp>
#include <string>
#include <stubDevice.hpp>
UTEST_STATE();
int main(int argc, const char *const argv[])
{
  return utest_main(argc, argv);
};

UTEST(deserialization, mainTest)
{
  MainAppStub testApp;
  testApp.start();
Logger::getLogger().log("MainTest", "End of test!");
}
