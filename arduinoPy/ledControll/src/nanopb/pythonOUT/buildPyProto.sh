pushd $(pwd)
current_dir=$1
echo $1
cd $current_dir
echo "Build Python Proto from $current_dir"
protoc -I=. --python_out=pythonOUT exProtocol.proto
popd
