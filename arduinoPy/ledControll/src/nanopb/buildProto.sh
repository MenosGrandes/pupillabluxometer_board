#!/bin/bash
cwd=$(pwd)
current_dir=$1
cd $current_dir

echo "Building exProto! from $current_dir"
protoc --plugin=generator/protoc-gen-nanopb --nanopb_out=. exProtocol.proto
# change name of the exProtocol.pb.c => exProtocol.c and for pb.h same
mv exProtocol.pb.h exProtocol.h
mv exProtocol.pb.c exProtocol.c
# change the include in exProtocol.pb.h from <pb.h> to "pb.h"
sed -i "s/[<|>]/\"/g" exProtocol.h
#change include in exProtocol.c
sed -i "s/exProtocol.pb.h/exProtocol.h/g" exProtocol.c
cd $cwd
