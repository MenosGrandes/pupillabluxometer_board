
#ifndef LED_IMPL_CPP
#define LED_IMPL_CPP

#include "Arduino.h"
#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <logger.cpp>

#include <FastLED.h>
#define NUM_LEDS 60
#define DATA_PIN 3
#define CLOCK_PIN 13
struct LedDevice : public DeviceIf<LedDevice, 12> {
  CRGB leds[NUM_LEDS];
  void init() { FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS); }

  void make() {
    for (int whiteLed = 0; whiteLed < NUM_LEDS; whiteLed = whiteLed + 1) {
      // Turn our current led on to white, then show the leds
      leds[whiteLed] = CRGB::White;

      // Show the leds (only one of which is set to white, from above)
      FastLED.show();

      // Wait a little bit
      delay(100);

      // Turn our current led back to black for the next loop around
      leds[whiteLed] = CRGB::Black;
    }

    Serial.println("a");
  }

};

#endif // !LED_IMPL_CPP
