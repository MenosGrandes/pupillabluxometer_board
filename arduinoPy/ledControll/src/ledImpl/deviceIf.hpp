#ifndef DEVICE_HPP
#define DEVICE_HPP
#include <stdint.h>
template<typename CONCRETE_DIVICE, uint8_t __descriptor,
         typename returnType = float>
struct DeviceIf {
  void init() {
    static_cast<CONCRETE_DIVICE*>(this)->init();
  }

  void make() {
    static_cast<CONCRETE_DIVICE*>(this)->make();
  }
  static constexpr auto Descriptor = __descriptor;
};

#endif // !DEVICE_HPP
