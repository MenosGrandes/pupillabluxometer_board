#ifndef STUB_DEVICE_HPP
#define STUB_DEVICE_HPP

#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <logger.cpp>
struct StubDevice : public DeviceIf<StubDevice, descriptors::DESCRIPTOR_STUB>
{
  void  init() {}
};

#endif // !STUB_DEVICE_HPP
