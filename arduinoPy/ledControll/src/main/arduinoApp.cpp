#ifdef ARDUINO
  #if ARDUINO >= 100
    #include "Arduino.h"
  #else
    #include "WProgram.h"
  #endif
#endif                           // ARDUINO
#include <appIf.cpp>
#include <exProtocolHandler.cpp>
#include <logger.cpp>
#include <serialMessageHandler.hpp>
#include <serialSender.hpp>
#include <stubDevice.hpp>
#include <timer.cpp>
#include <ledImpl.cpp>

/*
 *In main there have to be call to function init(). Arduino will not work
 *wihtout it!!!
 * */
struct ArduinoApp : public AppIf<ArduinoApp>
{
  int preInit()
  {
    init();
    return 0;
  }
  void start()
  {
    preInit();
    commonPart();
  }

  void commonPart()
  {

    static constexpr int                baundRate = 9600;
    LedDevice device;
    SerialSender                        sender;
    EXProtocolHandler<decltype(device)> protocol(&device);
    SerialMassageHandler<decltype(device), decltype(sender), decltype(protocol)>
        serialMsgHandler{&device, &sender, &protocol};

    sender.startSerialConnection(baundRate);
    device.init();

    Logger::getLogger().init();

    Timer<500, decltype(serialMsgHandler)> connectionTimer(
        &serialMsgHandler,
        &SerialMassageHandler<decltype(device), decltype(sender),
                              decltype(protocol)>::connect);
    while (true) {

      if (connected_t::NOT == serialMsgHandler.isConnected()) {
        connectionTimer.update();
      }

      if (Serial.available() > 0) {
        const auto messageSize = Serial.read();
        uint8_t    buffer[EXMessage_size];
        const auto bytesRead = Serial.readBytes(buffer, messageSize);
        Logger::getLogger().log("ArduinoApp",
                                "Data in serial. call for decodeMEssage!");
        serialMsgHandler.decodeMessage(messageSize, buffer);
      }

    device.make();
    }
  }
};
