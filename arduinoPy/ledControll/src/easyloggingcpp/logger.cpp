#ifndef LOGGER_HPP
#define LOGGER_HPP

#if defined(ARDUINO)
  #include "Arduino.h"
  #include <stdint.h>
  #include <stdlib.h>

  #ifdef SOFTWARE_SERIAL_DEBUG
    #include "SoftwareSerial.h"
SoftwareSerial SoftwareSerialDebug(10, 11); // RX, TX
  #endif

struct Logger
{
  static Logger &getLogger()
  {
    static Logger instance_;
    return instance_;
  }
  Logger() {}
  void init()
  {
  #ifdef SOFTWARE_SERIAL_DEBUG
    SoftwareSerialDebug.begin(9600);
    SoftwareSerialDebug.println("SoftwareSerialDebugeer online!");
  #endif
  }
  void createLogger(const char *) {}
  void log(const char *loggerName, const char *infoToLog, ...)
  {
  #ifdef DEBUG
    char    buffer[256];
    va_list args;
    va_start(args, infoToLog);
    vsnprintf(buffer, sizeof(buffer), infoToLog, args);
    va_end(args);
    buffer[sizeof(buffer) / sizeof(buffer[0]) - 1] = '\0';
    #ifdef SOFTWARE_SERIAL_DEBUG
    strcat(buffer, loggerName);
    SoftwareSerialDebug.println(buffer);
    #else
    Serial.println(buffer);
    #endif // !SOFTWARE_SERIAL_DEBUG
  #endif
  }

  void log_array(const char *loggerName, const char *infoToLog,
                 const uint8_t *array, const size_t size)
  {}
  Logger(Logger &&)      = delete;
  Logger(const Logger &) = delete;
  Logger &operator=(Logger &&) = delete;
  Logger &operator=(const Logger &) = delete;
  ~Logger()                         = default;
};
/**/
#else
  #include <lib/easylogging++.h>
  #ifndef LOGGER_INITIALIZED
INITIALIZE_EASYLOGGINGPP
    #define LOGGER_INITIALIZED
  #endif

  #include <iomanip>
  #include <iostream>
  #include <sstream>
class Logger
{
public:
  static Logger &getLogger()
  {
    static Logger instance_;
    return instance_;
  }
  Logger()
  {
    // Get Env Variable CPP_BUILD_FOLDER
    if (const char *exeLocation = std::getenv("CPP_BUILD_FOLDER")) {
      const auto fileLocation =
          std::string(std::string(exeLocation) + "logger.conf");
      el::Configurations conf(fileLocation);
      el::Loggers::reconfigureAllLoggers(conf);
    }
  }
  void log(const char *loggerName, const char *infoToLog, ...)
  {
    el::Logger *l = el::Loggers::getLogger(loggerName);
    char        buffer[512];
    va_list     args;
    va_start(args, infoToLog);
    vsprintf(buffer, infoToLog, args);
    l->info(buffer);
    va_end(args);
  }
  void log_array(const char *loggerName, const char *infoToLog,
                 const uint8_t *array, const size_t size)
  {

    el::Logger *      l = el::Loggers::getLogger(loggerName);
    std::stringstream ss;
    ss << "Logging ARRAY ";
    ss << std::hex;
    ss << "[ ";
    for (size_t i = 0; i < size; i++) {
      ss << std::setw(2) << "0x" << static_cast<unsigned>(array[i]) << " ";
    }
    ss << " ]";
    l->info(ss.str());
  }

  Logger(Logger &&)      = delete;
  Logger(const Logger &) = delete;
  Logger &operator=(Logger &&) = delete;
  Logger &operator=(const Logger &) = delete;
  ~Logger()                         = default;
};
#endif // !LOGGER_HPP
#endif
