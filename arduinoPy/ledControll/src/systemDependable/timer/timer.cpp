#ifndef TIMER_HPP
#define TIMER_HPP

#if defined(LINUX_PLATFORM)
  #include <chrono>
#endif

#if defined(ARDUINO)
  #if ARDUINO >= 100
    #include "Arduino.h"
    #define ARDUINO_PLATFORM
  #else
    #include "WProgram.h"
    #define ARDUINO_PLATFORM
  #endif
#endif

template <unsigned long INTERVAL, class T> class Timer
{
private:
  unsigned long _t;

public:
  Timer(T *callbackObject, bool (T::*callback)(void) const)
      : callbackObject(callbackObject)
      , timeout_callback(callback)
  {
    start();
  };
  Timer(Timer &&)      = default;
  Timer(const Timer &) = default;
  Timer &operator=(Timer &&) = default;
  Timer &operator=(const Timer &) = default;
  ~Timer()                        = default;

  void start()
  {
    _t = _millis();
  };

  unsigned int _millis()
  {

#if defined(LINUX_PLATFORM)
    std::chrono::time_point<std::chrono::system_clock> now =
        std::chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    auto millis =
        std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    return millis;

#elif defined(ARDUINO_PLATFORM)
    // Arduino have it's own millis
    return millis();
#endif
  }
  void update()
  {
    const unsigned long cycle_time = _millis();
    if (cycle_time - _t >= INTERVAL) {
      _t = cycle_time;
      (callbackObject->*timeout_callback)();
    }
  }

private:
  T *callbackObject;
  bool (T::*timeout_callback)(void) const;
};
#endif // !TIMER_HPP
