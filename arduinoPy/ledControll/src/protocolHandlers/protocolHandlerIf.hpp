
#ifndef PROTOCOL_HANDLER_IF_HPP
#define PROTOCOL_HANDLER_IF_HPP

#define PROPER_HANDLER_TYPE static_cast<CONCRETE_PROTOCOL_HANDLER *>(this)

#include <descriptors.hpp>
template <typename CONCRETE_PROTOCOL_HANDLER> class ProtocolHandlerIf {
public:
  ProtocolHandlerIf() = default;
  ProtocolHandlerIf(ProtocolHandlerIf &&) = default;
  ProtocolHandlerIf(const ProtocolHandlerIf &) = default;
  ProtocolHandlerIf &operator=(ProtocolHandlerIf &&) = default;
  ProtocolHandlerIf &operator=(const ProtocolHandlerIf &) = default;
  ~ProtocolHandlerIf() = default;

  response_types::response connect(uint8_t *buffer, size_t &size_of_message) {
    return PROPER_HANDLER_TYPE->connect(buffer, size_of_message);
  }

  response_types::response disconnect() {
    return PROPER_HANDLER_TYPE->disconnect();
  }

  response_types::response handleMessage(const EXMessage *msg,
                                         uint8_t *msgBuffer,
                                         size_t &messageLength) const {
    return PROPER_HANDLER_TYPE->handleMessage(msg, msgBuffer, messageLength);
  }

  response_types::response decodeMessage(uint8_t *msgBuffer,
                                         size_t &messageLength) const {
    return PROPER_HANDLER_TYPE->decodeMessage(msgBuffer, messageLength);
  }

  response_types::response encodeMessage(void *msg, uint8_t *buffer,
                                         size_t &size_of_message,
                                         bool needResponse) const {
    return PROPER_HANDLER_TYPE->encodeMessage(msg, buffer, size_of_message,
                                              needResponse);
  }

  connected_t isConnected() const { return PROPER_HANDLER_TYPE->isConnected(); }
};
#endif
