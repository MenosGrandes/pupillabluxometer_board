#ifndef DESCRIPTORS_HPP
#define DESCRIPTORS_HPP
#include <stdint.h>
namespace descriptors
{
constexpr uint8_t DESCRIPTORS_BASE                   = 0;
constexpr uint8_t DESCRIPTOR_TSL2591                 = 9;
constexpr uint8_t DESCRIPTOR_TSL2591X                = 2;
constexpr uint8_t DESCRIPTOR_GROVE_LIGHT_SENSOR_V1_1 = 3;
constexpr uint8_t DESCRIPTOR_GROVE_LIGHT_SENSOR_V1_0 = 4;
constexpr uint8_t DESCRIPTOR_TSL2561                 = 5;
constexpr uint8_t DESCRIPTOR_VEML7700                = 6;
constexpr uint8_t DESCRIPTOR_GY49                    = 7;
constexpr uint8_t DESCRIPTOR_STUB                    = 255;
}; // namespace descriptors
namespace commands
{
constexpr const char *OK              = "ok";
constexpr const char *RETURN_OK       = "r_ok";
constexpr const char *READ_LUX        = "rl";
constexpr const char *SEND_DESCRIPTOR = "ds";
constexpr const char *RESTART         = "res";
} // namespace commands
namespace timing
{
constexpr unsigned int DEFAULT_DELAY_MS = 500;
};
namespace frame_constants
{
constexpr uint8_t START_1 = 233;
constexpr uint8_t START_2 = 217;
constexpr uint8_t END_1   = 102;
constexpr uint8_t END_2   = 88;
}; // namespace frame_constants
namespace response_types
{

enum class response
{

  OK            = 1,
  NOK           = 2,
  SEND_RESPONSE = 3

};
}

enum class connected_t
{
  YES = 0,
  NOT = 1
};
#endif
