import re


def createDescriptors():
    # get namespace name, and create dict from it
    # get all inside items dict and create key,values
    regex = re.compile(
        "\\b(uint8_t|ifndef|endif|define|DESCRIPTORS_HPP|constexpr|const|byte|unsigned|int|char)\\W",
        re.I,
    )
    regex_2 = re.compile("{|}|;", re.I)

    fileName = "descriptors.py"
    desciptors_file = open(fileName, "w")
    desciptors_file.truncate()
    desciptors_file.write("from dataclasses import dataclass\n\n")
    desciptors_file.close()

    def saveToFile(name, dictionary):
        with open(fileName, "a") as py:
            py.write("@dataclass\n")
            py.write(f"class {name}:\n")
            for key, value in dictionary.items():
                py.write(f"    {key} = {value}\n")
            py.write("\n")

    def proceedNamespace(lines):
        match = re.search("namespace\s(\w+)", lines[0])

        if match:
            name = match.group(1)
            print(name)
            dictionary = dict()
            # next line is always the {
            lines = lines[2:]
            for k, namespace_member in enumerate(lines):
                if "}" in namespace_member:
                    print(dictionary)
                    saveToFile(name, dictionary)
                    return lines[k + 1 :]  # next is always };
                removed_cpp_words = regex.sub(
                    "", namespace_member
                )  # remove unused words
                removed_unused_chars = regex_2.sub("", removed_cpp_words).split(
                    "="
                )  # remove unused characters
                key_value_dict = [x.strip() for x in removed_unused_chars]
                dictionary[key_value_dict[0]] = key_value_dict[1]

    with open("../descriptors.hpp") as f:
        lines = f.readlines()[3:-1]  # FIRST TWO LINES ARE GUARDS SKIP THOSE
        lines = proceedNamespace(lines)
        # first namespace
        lines = proceedNamespace(lines)
        # second namespace
        lines = proceedNamespace(lines)
        # third namespace


createDescriptors()
