
#ifndef TYPE_TRAITS_HPP
#define TYPE_TRAITS_HPP

namespace mgtl {

template<typename N, N n>
struct integral_constant {
  using value_type = N;
  static constexpr value_type value = n;
  constexpr operator value_type() const noexcept { return value; }
  constexpr value_type operator()() const noexcept { return value; }
};

template<bool b>
using bool_constant = integral_constant<bool, b>;

using false_t = bool_constant<false>;
using true_t = bool_constant<true>;

/*
 *Forward
 *
 * */
template<typename T>
struct remove_const {
  using type = T;
};

template<typename T>
struct remove_const<const T> {
  using type = T;
};

template<typename T>
struct remove_const<const T&> {
  using type = T;
};

template<typename T>
struct remove_ptr {
  using type = T;
};

template<typename T>
struct remove_ptr<T*> {
  using type = T;
};

template<typename T>
struct remove_const<const T*> {
  using type = T;
};

template<typename T>
struct remove_reference {
  using type = T;
};

template<typename T>
struct remove_reference<T&> {
  using type = T;
};
template<typename T>
struct remove_reference<T&&> {
  using type = T;
};

template<typename T>
struct is_lvalue_reference : false_t {};

template<typename T>
struct is_lvalue_reference<T&> : true_t {};
template<class T>
constexpr T&&
forward(typename remove_reference<T>::type& t) noexcept {
  return static_cast<T&&>(t);
}

template<class T>
constexpr T&&
forward(typename remove_reference<T>::type&& t) noexcept {
  static_assert(!is_lvalue_reference<T>::value,
                "Can not forward an rvalue as an lvalue.");
  return static_cast<T&&>(t);
}

template<typename T, typename R>
struct is_same : false_t {};
template<typename T>
struct is_same<T, T> : true_t {};

template<typename T, typename R>
constexpr bool is_same_v = is_same<T, R>::value;

static_assert(is_same_v<int, int>, "is_same does not work!");

template<bool B, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T> {
  using type = T;
};

template<bool B, class T = void>
using enable_if_t = typename enable_if<B, T>::type;

template<typename T, typename R>
using enable_if_same_t = typename mgtl::enable_if_t<mgtl::is_same_v<T, R>, int>;
//Example usage
// template<typename tag_t, typename = mgtl::enable_if_same_t<tag::EXMessageSetupResponseTag, tag_t> >
}

#endif
