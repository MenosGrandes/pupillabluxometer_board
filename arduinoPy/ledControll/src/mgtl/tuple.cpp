#ifndef tuple_HPP
#define tuple_HPP
#include "type_traits.cpp"
namespace mgtl
{

template<typename... T>
struct tuple
{
};

template<typename T,
         typename... Rest // Template parameter pack
         >
struct tuple<T, Rest...>
{ // Class parameter pack
  T first;
  tuple<Rest...> rest; // Parameter pack expansion

  tuple(const T& f, const Rest&... r) : first(f), rest(r...) {}
};

};

#endif
