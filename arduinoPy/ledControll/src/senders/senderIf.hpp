#ifndef SENDER_IF
#define SENDER_IF
#include <stddef.h>
#include <stdint.h>
template<typename CONCRETE_SENDER>
struct SenderIf
{
    bool startSerialConnection(uint32_t baundRate)
    {
        return static_cast<CONCRETE_SENDER*>(this)->startSerialConnection(baundRate);
    }
    bool sendBuffer(uint8_t* buffer, uint8_t bufferSize) const
    {
        return static_cast<CONCRETE_SENDER*>(this)->sendBuffer(buffer,
                                                               bufferSize);
    }
};
#endif // !SENDER_IF
