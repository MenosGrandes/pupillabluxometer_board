#ifndef MESSAGE_HANDLER_HPP
#define MESSAGE_HANDLER_HPP
#include <descriptors.hpp>
#include <deviceIf.hpp>
#include <logger.cpp>
#include <messageHandlerIf.hpp>
#include <protocolHandlerIf.hpp> //Should be in seperate IF MenosGrandse
template <class DeviceIf, class SenderIf, class ProtocolIf>
class SerialMassageHandler
    : public MessageHandler<
          SerialMassageHandler<DeviceIf, SenderIf, ProtocolIf>, DeviceIf,
          SenderIf, ProtocolIf>
{
public:
  SerialMassageHandler(DeviceIf *_device, SenderIf *_sender,
                       ProtocolIf *_protocol)
      : device(_device)
      , sender(_sender)
      , protocol(_protocol){};
  SerialMassageHandler(SerialMassageHandler &&)      = default;
  SerialMassageHandler(const SerialMassageHandler &) = default;
  SerialMassageHandler &operator=(SerialMassageHandler &&) = default;
  SerialMassageHandler &operator=(const SerialMassageHandler &) = default;
  ~SerialMassageHandler()                                       = default;

public:
  bool decodeMessage(size_t  messageLength,
                     uint8_t msgBuffer[ProtocolIf::MAX_MSG_SIZE]) const
  {

    const auto responseType = protocol->decodeMessage(msgBuffer, messageLength);

    if (responseType == response_types::response::SEND_RESPONSE) {
      return sender->sendBuffer(msgBuffer, messageLength);
    }
    else if (responseType == response_types::response::OK) {
      return true;
    }

    return false;
  }
  bool connect() const
  {
    if (isConnected() == connected_t::NOT) {
      uint8_t buffer[ProtocolIf::MAX_MSG_SIZE]; // MenosGrandes it needs to be
                                                // here.. for some reason.
      size_t     size_of_message{0};
      const auto response = protocol->connect(buffer, size_of_message);
      if (response == response_types::response::SEND_RESPONSE) {
        if (sender->sendBuffer(buffer, size_of_message)) {
          return true;
        }
      }
    }
    return false;
  }

  bool disconnect()
  {
    return protocol->disconnect();
  };

  connected_t isConnected() const
  {
    return protocol->isConnected();
  }
  const DeviceIf *device{nullptr};
  SenderIf *      sender{nullptr};
  ProtocolIf *    protocol{nullptr};
};

#endif // !MESSAGE_HANDLER_HPP
