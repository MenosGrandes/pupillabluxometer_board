#ifndef MISC_HPP
#define MISC_HPP

#if __cpp_sized_deallocation
#include <Arduino.h>
void operator delete(void* ptr, size_t)
{
  delete ptr;
}

void operator delete[](void* ptr, size_t)
{
  delete[] ptr;
}

#endif

#endif // MISC_HPP
