#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
    Serial.println("Goodnight moon!");
  mySerial.begin(9600); 
}

void loop() {
      if (mySerial.available())
      {
        Serial.write(mySerial.read());
      }

}
